import generateIssue from "./generate_issue";

export default (amount = 10, options = {}) =>
  Array(amount)
    .fill("x")
    .map((_, i) => generateIssue(i, options));
