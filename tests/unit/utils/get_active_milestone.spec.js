import getActiveMilestone from "@/utils/get_active_milestone";

describe("getActiveMilestone", () => {
  it.each`
    milestone  | date
    ${"13.0"}  | ${new Date(2020, 3, 15)}
    ${"13.0"}  | ${new Date(2020, 4, 14)}
    ${"13.1"}  | ${new Date(2020, 5, 1)}
    ${"13.8"}  | ${new Date(2020, 11, 31)}
    ${"13.8"}  | ${new Date(2021, 0, 2)}
    ${"13.11"} | ${new Date(2021, 2, 20)}
    ${"13.11"} | ${new Date(2021, 3, 14)}
    ${"14.0"}  | ${new Date(2021, 3, 20)}
  `("should get $milestone for $date", ({ milestone, date }) => {
    expect(getActiveMilestone(date)).toEqual(milestone);
  });
});
