import getMilestoneEndDate from "@/utils/get_milestone_end_date";

describe("getMilestoneEndDate", () => {
  it.each`
    milestone  | endYear | endMonth
    ${"12.10"} | ${2020} | ${"April"}
    ${"13.0"}  | ${2020} | ${"May"}
    ${"13.1"}  | ${2020} | ${"June"}
    ${"13.7"}  | ${2020} | ${"December"}
    ${"13.8"}  | ${2021} | ${"January"}
    ${"14.0"}  | ${2021} | ${"May"}
    ${"14.10"} | ${2022} | ${"March"}
  `(
    "should get $endMonth $endYear for the $milestone milestone",
    ({ milestone, endYear, endMonth }) => {
      const result = getMilestoneEndDate(milestone);

      expect(result.toLocaleString("en-US", { month: "long" })).toEqual(
        endMonth
      );
      expect(result.getFullYear()).toEqual(endYear);
      expect(result.getDate()).toEqual(18);
    }
  );
});
