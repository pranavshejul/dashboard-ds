import { shallowMount } from "@vue/test-utils";
import CurrentUser from "@/components/current_user.vue";

let wrapper;
const defaultProps = {
  avatarUrl: "http://placecage.com/300",
  username: "nick_cage",
};

describe("Current user", () => {
  beforeEach(() => {
    wrapper = shallowMount(CurrentUser, {
      propsData: defaultProps,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("renders the alt text", () => {
    const altText = `Avatar of ${defaultProps.username}`;
    expect(wrapper.find("img").attributes().alt).toEqual(altText);
  });

  describe('avatar url', () => {
    it('should get the absolute avatar URL', () => {
      expect(wrapper.find("img").attributes().src).toEqual(
        defaultProps.avatarUrl
      );
    });

    it('should turn relative path to absolute path', () => {
      const relativePath = '/relative/path';
      wrapper = shallowMount(CurrentUser, {
        propsData: {
          ...defaultProps,
          avatarUrl: relativePath,
        }
      });

      expect(wrapper.find("img").attributes().src).toEqual(
        `https://www.gitlab.com${relativePath}`
      );
    });
  });
});
