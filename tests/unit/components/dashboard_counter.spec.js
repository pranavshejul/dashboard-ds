import { shallowMount } from "@vue/test-utils";
import DashboardCounter from "@/components/dashboard_counter.vue";

let wrapper;
const defaultProps = { count: 10, title: "Title" };

describe("Dashboard Counter", () => {
  beforeEach(() => {
    wrapper = shallowMount(DashboardCounter, {
      propsData: defaultProps,
    });
  });

  afterEach(() => {
    wrapper.destroy();
  });

  it("renders the title", () => {
    expect(wrapper.find("h2").text()).toEqual(defaultProps.title);
  });

  it("renders the count", () => {
    expect(wrapper.find("strong").text()).toEqual(
      defaultProps.count.toString()
    );
  });
});
