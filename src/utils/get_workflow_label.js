import { WORKFLOW_LABEL_PREFIX } from "@/constants";

export default (labels = []) =>
  labels.find(({ title }) =>
    title.match(new RegExp(`^${WORKFLOW_LABEL_PREFIX}`))
  )?.title || "";
