export default (issues = []) => {
  const relevantIssues = issues;

  return (
    relevantIssues.reduce((acc, { completion }) => acc + completion, 0) /
    relevantIssues.length
  );
};
